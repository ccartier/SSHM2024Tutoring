const parameters = {};
window.location.search.substr(1).split("&").forEach(item => {
	parameters[item.split("=")[0]] = item.split("=")[1];
});
console.log(parameters);
const OBLIQUITY = "23.5°";
const DEFAULT_LATITUDE = "59°";

const STEP_AL = parseInt("almucantars" in parameters ? parameters["almucantars"] : 30);
const STEP_AZIMUTH = parseInt("azimuths" in parameters ? parameters["azimuths"] : 30);

const ASTROLABES = [];

class Drawing {
	get color() {
		return undefined === this._color ? [0, 0, 0] : this._color;
	}

	get elements() {
		return [...this.solid_elements, ...this.scafoldings, ...this.drawing_lines];
	}

	get solid_elements() {
		return [];
	}

	get scafoldings() {
		return [];
	}

	get drawing_lines() {
		return [];
	}

	get subdrawings() {
		return [];
	}

	get subscafoldings() {
		return [];
	}

	get is_visible() {
		return undefined === this._is_visible ? true : this._is_visible;
	}

	get are_scafoldings_shown() {
		return undefined === this._are_scafoldings_shown ? true : this._are_scafoldings_shown;
	}

	constructor(geogebraAPI) {
		this.ggb = geogebraAPI;
	}

	redraw() {
		this.toggleShow(this.is_visible);
		this.toggleScafoldings(this.are_scafoldings_shown);

		return this;
	}

	toggleShow(show, propagate=true) {
		this.setColor(...this.color, false);
		this._is_visible = show;

		for (const obj of [...this.solid_elements]) {
			this.ggb.setVisible(obj, show);
			this.ggb.setLabelVisible(obj, "point" === this.ggb.getObjectType(obj));
			this.ggb.setLineStyle(obj, 0);
			this.ggb.setAuxiliary(obj, !show);
		}

		if (true === propagate) {
			for (const obj of [...this.subdrawings]) {
				obj.toggleShow(show);
			}
		}

		if (false === show) {
			this.toggleScafoldings(false, propagate);
		}

		return this;
	}

	toggleScafoldings(show, propagate=true) {
		this.setColor(...this.color, false);
		this._are_scafoldings_visible = show;

		for (const obj of this.scafoldings) {
			this.ggb.setLineStyle(obj, 1);
			this.ggb.setLabelVisible(obj, "point" === this.ggb.getObjectType(obj) && show);
			this.ggb.setVisible(obj, show);
			this.ggb.setAuxiliary(obj, !show);
		}

		for (const obj of this.drawing_lines) {
			this.ggb.setLineStyle(obj, 3);
			this.ggb.setLabelVisible(obj, "point" === this.ggb.getObjectType(obj) && show);
			this.ggb.setVisible(obj, show);
			this.ggb.setAuxiliary(obj, !show);
		}

		if (true === propagate) {
			for (const obj of [...this.subdrawings, ...this.subscafoldings]) {
				obj.toggleScafoldings(show);
			}

			for (const obj of [...this.subscafoldings]) {
				obj.toggleShow(show);
				for (const j of obj.solid_elements) {
					this.ggb.setLabelVisible(j, "point" === this.ggb.getObjectType(j) && show);
					this.ggb.setLineStyle(j, 1);
				}
			}
		}

		if (true === show) {
			this.toggleShow(true, propagate);
		}

		return this;
	}

	setColor(red, green, blue, propagate=true) {
		this._color = [red, green, blue];

		for (const obj of [...this.elements]) {
			this.ggb.setColor(obj, red, green, blue);
		}

		if (true !== propagate) {
			return;
		}

		for (const obj of [...this.subdrawings, ...this.subscafoldings]) {
			obj.setColor(red, green, blue);
		}

		return this;
	}
}

class Astrolabe extends Drawing {
	get subdrawings() {
		return [this.plate, this.spider];
	}

	get radius() {
		return this._radius;
	}

	constructor(ggb, radius) {
		super(ggb);
		this.ggb.evalCommand("radius="+radius);
		this.ggb.evalCommand("obliquity=Slider(0°, 90°, .1°)");
		this.ggb.evalCommand("SetValue(obliquity, "+OBLIQUITY+")");
		this.plate = new Plate(ggb, radius);
		this.spider = new Spider(ggb, radius);
		this.spider.setColor(226, 121, 29);
		this.plate.setColor(36, 117, 101);
		ASTROLABES.push(this);
	}
}

class Plate extends Drawing {
	get solid_elements() {
		return [
			"E_{plate}",
			"Capricorn_{plate}",
			"A_{plate}",
			"J_{plate}",
			"B_{plate}",
			"D_{plate}",
			"AJ_{plate}",
			"BD_{plate}",
		];
	}

	get scafoldings() {
		return [];
	}

	get drawing_lines() {
		return [];
	}

	get subdrawings() {
		return [
			this.equator,
			// this.cancer,
			this.horizon,
			this.zenith,
			...this.almucantars,
			this.firstVertical,
			...this.azimuths,
		];
	}

	constructor(ggb, radius) {
		super(ggb);
		this.ggb.evalCommand("E_{plate}=Intersect(xAxis, yAxis)");
		this.ggb.evalCommand("Capricorn_{plate}=Circle(E_{plate}, radius)");
		this.ggb.evalCommand("A_{plate}=Intersect(yAxis, Capricorn_{plate}, 2)");
		this.ggb.evalCommand("J_{plate}=Intersect(yAxis, Capricorn_{plate}, 1)");
		this.ggb.evalCommand("B_{plate}=Intersect(xAxis, Capricorn_{plate}, 1)");
		this.ggb.evalCommand("D_{plate}=Intersect(xAxis, Capricorn_{plate}, 2)");
		this.ggb.evalCommand("AJ_{plate}=Line(A_{plate}, J_{plate})");
		this.ggb.evalCommand("BD_{plate}=Line(B_{plate}, D_{plate})");

		this.ggb.evalCommand("latitude=Slider(0°,90°)");
		this.ggb.evalCommand("SetValue(latitude, 55°)");

		this.equator = new Equator(ggb, "plate");
		// this.cancer = new Cancer(ggb, "plate");
		this.horizon = new Horizon(ggb);
		this.zenith = new Zenith(ggb);
		this.almucantars = [];
		for (let altitude = STEP_AL; altitude < 90; altitude += STEP_AL) {
			this.almucantars.push(new Almucantar(ggb, altitude));
		}
		this.firstVertical = new FirstVertical(ggb);
		this.azimuths = [];
		for (let azimuth = STEP_AZIMUTH; azimuth < 90; azimuth += STEP_AZIMUTH) {
			this.azimuths.push(new Azimuth(ggb, azimuth));
			this.azimuths.push(new Azimuth(ggb, -azimuth));
		}
	}
}

class Spider extends Drawing {
	get solid_elements() {
		return [
			"E_{spider}",
			"Capricorn_{spider}",
			"A_{spider}",
			"J_{spider}",
			"B_{spider}",
			"D_{spider}",
			"AJ_{spider}",
			"BD_{spider}",
		];
	}

	get subdrawings() {
		return [this.equator, this.cancer, this.ecliptic, ...this.stars];
	}

	constructor(ggb, radius) {
		super(ggb);
		this.ggb.evalCommand("E_{spider}=Intersect(xAxis, yAxis)");
		this.ggb.evalCommand("Capricorn_{spider}=Circle(E_{spider}, radius)");
		this.ggb.evalCommand("A_{spider}=Point(Capricorn_{spider})");
		this.ggb.evalCommand("J_{spider}=Intersect(Line(E_{spider}, A_{spider}), Capricorn_{spider}, 2)");
		this.ggb.evalCommand("B_{spider}=Rotate(A_{spider}, 90°)");
		this.ggb.evalCommand("D_{spider}=Rotate(J_{spider}, 90°)");
		this.ggb.evalCommand("AJ_{spider}=Segment(A_{spider}, J_{spider})");
		this.ggb.evalCommand("BD_{spider}=Segment(B_{spider}, D_{spider})");

		this.equator = new Equator(ggb, "spider");
		this.cancer = new Cancer(ggb, "spider");
		this.ecliptic = new Ecliptic(ggb, "spider");
		this.stars = [];
	}
}

class Equator extends Drawing {
	get solid_elements() {
		return [
			"equator_{"+this.struct+"}",
		];
	}

	get scafoldings() {
		return [
			"Z_{"+this.struct+"}",
			"H_{"+this.struct+"}",
			"AZ_{"+this.struct+"}",
			"K_{"+this.struct+"}",
			"Y_{"+this.struct+"}",
			"N_{"+this.struct+"}",
		];
	}

	constructor(ggb, structure) {
		super(ggb);
		this.struct = structure;
		const dj = "Arc(Capricorn_{"+structure+"}, J_{"+structure+"}, D_{"+structure+"})";
		const obliquity = "Rotate(Line(D_{"+structure+"}, E_{"+structure+"}), -obliquity)";
		this.ggb.evalCommand("Z_{"+structure+"}=Intersect("+dj+", "+obliquity+")");
		this.ggb.evalCommand("AZ_{"+structure+"}=Segment(A_{"+structure+"}, Z_{"+structure+"})");
		this.ggb.evalCommand("H_{"+structure+"}=Intersect(AZ_{"+structure+"}, Segment(E_{"+structure+"}, D_{"+structure+"}))");
		this.ggb.evalCommand("equator_{"+this.struct+"}=Circle(E_{"+this.struct+"}, H_{"+this.struct+"})");
		this.ggb.evalCommand("K_{"+this.struct+"}=Intersect(equator_{"+this.struct+"}, Ray(E_{"+this.struct+"}, A_{"+this.struct+"}))");
		this.ggb.evalCommand("N_{"+this.struct+"}=Intersect(equator_{"+this.struct+"}, Ray(E_{"+this.struct+"}, J_{"+this.struct+"}))");
		this.ggb.evalCommand("Y_{"+this.struct+"}=Intersect(equator_{"+this.struct+"}, Ray(E_{"+this.struct+"}, B_{"+this.struct+"}))");
	}
}

/** Requires the equator to be drawn before **/
class Cancer extends Drawing {
	get solid_elements() {
		return ["Cancer_{"+this.struct+"}"];
	}
	get scafoldings() {
		return [
			"EZ_{"+this.struct+"}",
			"T_{"+this.struct+"}",
			"KT_{"+this.struct+"}",
			"L_{"+this.struct+"}",
			"S_{"+this.struct+"}",
			// Defined by the equator
			"Z_{"+this.struct+"}",
			"K_{"+this.struct+"}",
		];
	}

	constructor(ggb, structure) {
		super(ggb);
		this.struct = structure;
		this.ggb.evalCommand("EZ_{"+this.struct+"}=Segment(E_{"+this.struct+"}, Z_{"+this.struct+"})");
		this.ggb.evalCommand("T_{"+this.struct+"}=Intersect(EZ_{"+this.struct+"}, equator_{"+this.struct+"})");
		this.ggb.evalCommand("KT_{"+this.struct+"}=Segment(K_{"+this.struct+"}, T_{"+this.struct+"})");
		this.ggb.evalCommand("L_{"+this.struct+"}=Intersect(KT_{"+this.struct+"}, Ray(D_{"+this.struct+"}, E_{"+this.struct+"}))");
		this.ggb.evalCommand("Cancer_{"+this.struct+"}=Circle(E_{"+this.struct+"}, L_{"+this.struct+"})");
		this.ggb.evalCommand("S_{"+this.struct+"}=Intersect(Cancer_{"+this.struct+"}, Ray(E_{"+this.struct+"}, B_{"+this.struct+"}))");
	}
}

/** Requires the Tropic of Cancer to be drawn before **/
class Ecliptic extends Drawing {
	get solid_elements() {
		return ["ecliptic_{"+this.struct+"}"];
	}
	get scafoldings() {
		return [
			"X_{"+this.struct+"}",
			// Defined by the Cancer
			"S_{"+this.struct+"}",
		];
	}
	constructor(ggb, structure) {
		super(ggb);
		this.struct = structure;
		this.ggb.evalCommand("X_{"+this.struct+"}=Midpoint(D_{"+this.struct+"}, S_{"+this.struct+"})");
		this.ggb.evalCommand("ecliptic_{"+this.struct+"}=Circle(X_{"+this.struct+"}, S_{"+this.struct+"})");
	}
}

/** Requires the Equator to be drawn before **/
class Horizon extends Drawing {

	get solid_elements() {
		return ["Horizon"];
	}
	get scafoldings() {
		return [
			"L_{horizon}",
			"M_{horizon}",
			"LM_{horizon}",
			"Z_{horizon}",
			"T_{horizon}",
			"ZT_{horizon}",
			"YO_{horizon}",
			"O_{horizon}",
			// Defined on the Equator
			"Y_{plate}",
		];
	}
	constructor(ggb) {
		super(ggb);
		this.ggb.evalCommand("L_{horizon}=Rotate(Y_{plate}, -latitude)");
		this.ggb.evalCommand("M_{horizon}=Intersect(Ray(L_{horizon}, E_{plate}), equator_{plate},2)");
		this.ggb.evalCommand("LM_{horizon}=Segment(L_{horizon}, M_{horizon})");
		this.ggb.evalCommand("Z_{horizon}=Rotate(M_{horizon}, 90°)");
		this.ggb.evalCommand("T_{horizon}=Rotate(L_{horizon}, 90°)");
		this.ggb.evalCommand("ZT_{horizon}=Segment(Z_{horizon}, T_{horizon})");
		this.ggb.evalCommand("YO_{horizon}=PerpendicularLine(Y_{plate}, LM_{horizon})");
		this.ggb.evalCommand("O_{horizon}=Intersect(YO_{horizon}, Ray(E_{plate}, A_{plate}))");
		this.ggb.evalCommand("Horizon=Circle(O_{horizon}, Y_{plate})");
	}
}

/** Requires the Horizon to be drawn before **/
class Zenith extends Drawing {
	get solid_elements() {
		return ["C_{zenith}"];
	}
	get scafoldings() {
		return [
			"ZY_{zenith}",
			// Defined through the horizon
			"Z_{horizon}",
			"T_{horizon}",
			"ZT_{horizon}",
			"YO_{horizon}",
			"O_{horizon}",
			// Defined on the Equator
			"Y_{plate}",
		];
	}
	constructor(ggb) {
		super(ggb);
		this.ggb.evalCommand("ZY_{zenith}=Segment(Z_{horizon}, Y_{plate})");
		this.ggb.evalCommand("C_{zenith}=Intersect(ZY_{zenith}, Line(A_{plate}, J_{plate}))");
	}

}

class Star extends Drawing {
	constructor(ggb, structure) {
		super(ggb);
		this.struct = structure;
	}
}

/** Requires the Horizon to be drawn before **/
class Almucantar extends Drawing {
	get solid_elements() {
		return ["al_{"+this.alt+"}"];
	}

	get scafoldings() {
		return [
			"Z'_{al"+this.alt+"}",
			"Z''_{al"+this.alt+"}",
			"YZ'_{al"+this.alt+"}",
			"YZ''_{al"+this.alt+"}",
			"X'_{al"+this.alt+"}",
			"X''_{al"+this.alt+"}",
			"C_{al"+this.alt+"}",
		];
	}
	constructor(ggb, altitude) {
		super(ggb);
		this.alt = altitude;
		this.ggb.evalCommand("Z'_{al"+this.alt+"}=Rotate(Z_{horizon}, "+(90-this.alt)+"°)");
		this.ggb.evalCommand("Z''_{al"+this.alt+"}=Rotate(Z_{horizon}, -"+(90-this.alt)+"°)");
		this.ggb.evalCommand("YZ'_{al"+this.alt+"}=Ray(Y_{plate}, Z'_{al"+this.alt+"})");
		this.ggb.evalCommand("YZ''_{al"+this.alt+"}=Ray(Y_{plate}, Z''_{al"+this.alt+"})");
		this.ggb.evalCommand("X'_{al"+this.alt+"}=Intersect(YZ'_{al"+this.alt+"}, Line(A_{plate}, J_{plate}))");
		this.ggb.evalCommand("X''_{al"+this.alt+"}=Intersect(YZ''_{al"+this.alt+"}, Line(A_{plate}, J_{plate}))");
		this.ggb.evalCommand("C_{al"+this.alt+"}=Midpoint(X'_{al"+this.alt+"}, X''_{al"+this.alt+"})");
		this.ggb.evalCommand("al_{"+this.alt+"}=Circle(C_{al"+this.alt+"}, X'_{al"+this.alt+"})");
	}
}

/** Requires the Zenith to be dranw before **/
class FirstVertical extends Drawing {
	get solid_elements() {
		return ["FirstVertical"];
	}

	get drawing_lines() {
		return [
			"Centers_{azimuths}",
		];
	}

	get scafoldings() {
		return [
			"YV_{az}",
			"V_{az}",
			// Defined in the horizon
			"ZT_{horizon}",
			// Defined by the Zenith
			"C_{zenith}",
		];
	}

	constructor(ggb) {
		super(ggb);
		this.ggb.evalCommand("YV_{az}=PerpendicularLine(Y_{plate}, ZT_{horizon})");
		this.ggb.evalCommand("V_{az}=Intersect(YV_{az}, AJ_{plate})");
		this.ggb.evalCommand("FirstVertical=Circle(V_{az}, C_{zenith})");
		this.ggb.evalCommand("Centers_{azimuths}=PerpendicularLine(V_{az}, Line(A_{plate}, J_{plate}))");
	}
}

class Azimuth extends Drawing {
	get solid_elements() {
		return ["Az_{"+this.az+"}"];
	}
	get scafoldings() {
		return [
			"CW_{az"+this.az+"}",
			"W_{az"+this.az+"}",
			// Defined by the first vertical
			"V_{az}",
			"Centers_{azimuths}",
		];
	}
	constructor(ggb, azimuth) {
		super(ggb);
		this.az = azimuth;
		this.ggb.evalCommand("CW_{az"+this.az+"}=Ray(C_{zenith}, Rotate(V_{az}, "+this.az+"°, C_{zenith}))");
		this.ggb.evalCommand("W_{az"+this.az+"}=Intersect(CW_{az"+this.az+"}, Centers_{azimuths})");
		this.ggb.evalCommand("Az_{"+this.az+"}=Circle(W_{az"+this.az+"}, C_{zenith})");
	}
}

let params = {
	"appName": "geometry",
	"width": 1200,
	"height": 800,
	"showAlgebraInput": true,
	"appletOnLoad": function(ggb) {
		const astrolabe = new Astrolabe(ggb, 7.5);
		astrolabe.toggleScafoldings(false);

		const controls = document.getElementById("controls").content.cloneNode(true);
		const controls_div = document.getElementById("controls-container");
		controls_div.textContent = "";
		controls_div.appendChild(controls);
		controls_div.querySelectorAll("button").forEach(btn => btn.addEventListener("click", describe));

		document.querySelector("canvas[data-test=euclidianView]").focus();
	}
};

let applet = new GGBApplet(params, true);
window.addEventListener("load", function() {
	applet.inject(document.getElementById("ggb-element"));
});


function describe(event) {
	const btn = event.target;
	document.getElementById("description").innerHTML = "<h3>"+btn.textContent+"</h3><p>"+btn.title+"</p>";
}
