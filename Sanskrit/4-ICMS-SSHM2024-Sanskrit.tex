% Created 2024-07-27 sam. 20:13
% Intended LaTeX compiler: pdflatex
\documentclass[12pt]{beamer}
\usepackage[LAE, LGR, T2A, T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage[american]{babel}
\usepackage{imakeidx}
\usepackage{textalpha}
\usepackage{arabtex}
\usepackage{utf8}
\usepackage[newfloat]{minted}
\usepackage[hidelinks]{hyperref}
\usepackage{wasysym}
\usepackage{setspace}
\newcommand{\fname}[1]{\textsc{#1}}
\setbeameroption{hide notes}
\usetheme{default}
\author{Irem ASLAN-SEYHAN (University of Bartın) \newline Clément CARTIER (Université Paris Cité, CNRS, SPHERE)}
\date{\textit{<2024-07-25 jeu.>}}
\title{Proofs of the Pythagorean theorem as in Bhāskara II's Algebra}
\institute{Edinburgh, Summer School in the History of Mathematics\\ Current research on the history of mathematics in the ancient world: new questions and new approaches}
\usepackage{caption}
\usepackage{subcaption}
\usetheme{icms}
\AtBeginSection[]{\begin{frame}<beamer>\frametitle{Plan}\tableofcontents[currentsection, currentsubsection]\end{frame}}
\hypersetup{
 pdfauthor={Irem ASLAN-SEYHAN (University of Bartın) \newline Clément CARTIER (Université Paris Cité, CNRS, SPHERE)},
 pdftitle={Proofs of the Pythagorean theorem as in Bhāskara II's Algebra},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.6-pre)}, 
 pdflang={English}}
\makeatletter
\newcommand{\citeprocitem}[2]{\hyper@linkstart{cite}{citeproc_bib_item_#1}#2\hyper@linkend}
\makeatother

\usepackage[notquote]{hanging}
\begin{document}

\maketitle


\section{\emph{Víjagańita}}
\label{sec:orgdf56473}

\begin{frame}[label={sec:orga67e5e1}]{Context}
\begin{tikzpicture}[scale=1.3]
\fill[icms!10!white] (4,0) rectangle (21, 1);
\foreach \i in {4, ..., 20}{
    \draw[icms] (\i, 0) node[anchor=north] {\i00};
    \draw[lightgray!10!white] (\i, 0) -- (\i, 1);
}

\fill[icms] (4.8, 0) rectangle (5.5, 1);
\draw[icms] (4.8, 1) node[anchor=south] {Aryabhata I};

\fill[icms] (6, 0) rectangle (6.8, 1);
\draw[icms] (6.8, 1) node[anchor=south] {Brahmagupta};

\draw[icms] (11.5, 0) -- (11.5, 1) node[anchor=south] {Víjagańita};

\fill[icms] (15, 0) rectangle (16, 1);
\draw[icms] (15.5, 1) node[anchor=south] {Bījapallavam};

\draw[icms] (18.1, 0) -- (18.1, 1) node[anchor=south] {Colebrooke};

\draw[icms] (20.2, 0) -- (20.2, 1) node[anchor=south] {Summer School};

\draw[black] (4, 0) rectangle (21, 1);
\end{tikzpicture}

\note{Context
\begin{quote}
The treatises in question, which occupy the present volume, are the \emph{Víjagańita} and the \emph{Lílávatí} of \textsc{Bháscara áchárya} and the \emph{Gańitád'haya} and \emph{Cuttacád'haya} of \textsc{Brahmegupta}.
The two first mentioned constitute the preliminary portion of \textsc{Bháscara}'s Course of Astronomy, entitled \emph{Sidd'hántaśirómańi}.
The two last are the twelfth and eighteenth chapters of a similar course of astronomy, by \textsc{Brahmegupta}, entitled \emph{Brahma-sidd'hánta}.
\end{quote}
(Colebrooke 1817, p.ii)

Our text is from chapter V of Bháscara's \emph{Víja-gańita}, dedicated to ``Quadratic, \&c. Equations'' (Colebrooke's title)

\begin{quote}
The date of the \emph{Sidd'hánta-śirómańi}, of which the \emph{Víja-gańita} and \emph{Lílávatí} are parts, is fixt then with the utmost exactness, on the most satisfactory grounds, at the middle of the twelfth century of the Christian era, A.D. 1150.
\end{quote}
(Colebrooke 1817, p.iii)

\begin{quote}
It will be also shown, that \textsc{Bháscara}, who himself flourished more than six hundred and fifty years ago was in this respect a compiler, and took those methods from Indian authors as much more ancient than himself.
\end{quote}
(Colebrooke 1817, p.iv)}
\end{frame}

\begin{frame}[label={sec:org77eeaee}]{Contents}
\begin{center}
\emph{Víjagańita}
\end{center}
\begin{columns}
\begin{column}{.5\columnwidth}
\begin{enumerate}
\item Algorithm or Logistics
\begin{enumerate}
\item Invocation and Introduction
\item Algorithm of Negative and Affirmative Quantities
\item Algorithm of Cipher
\item Algorithm of Unknown Quantity
\item Algorithm of Surds
\end{enumerate}
\item Pulverizer
\item Affected Square
\begin{enumerate}
\item Affected Square
\item Cyclic Method
\item Miscellaneous
\end{enumerate}
\end{enumerate}
\end{column}
\begin{column}{.5\columnwidth}
\begin{enumerate}
\item Simple Equation
\item \alert{Quadratic, \&c. Equations}
\item Multilateral Equations
\item Varieties of Quadratics
\item Equation involving a Factum of Unknown Quantities
\item Conclusion
\end{enumerate}
\end{column}
\end{columns}

\note{Plan of the work
Algebra (\emph{Víja-gańita})
\begin{enumerate}
\item\relax […]
\begin{enumerate}
\item Invocation \&c
\item Algorithm of Negative and Affirmative Quantities
\item Algorithm of Cipher
\item Algorithm of Unknown Quantity
\item Algorithm of Surds
\end{enumerate}
\item Pulverizer
\item Affected Square
\begin{enumerate}
\item ?
\item Cyclic Method
\item Miscellaneous
\end{enumerate}
\item Simple Equation
\item Quadratic, \&c. Equations
\begin{quote}
Next equation involving the square or other [power] of the unknown is propounded.
[Its re-solution consists in] the elimination of the middle term\footnote{\emph{Mad'hyamáharańa}; from \emph{mad'hyama} middlemost, and \emph{áharańa} a taking away or (\emph{apanayana}) removal. |…}], as teachers of the sicence\footnote{\emph{Ácháryas}; ancient mathematicians (\emph{ád})} denominate it.
Here removal of one term, the middle one, in the square quantity, takes place: wherefore it is called elimination of the middle term.
\end{quote}
\item Multilateral Equations
\item Varieties of Quadratics
\item Equation involving a Factum of Unknown Quantities
\item Conclusion
\end{enumerate}}
\end{frame}

\begin{frame}[label={sec:org32cfc58}]{Quadratic equations}
\begin{columns}
\begin{column}{.5\columnwidth}
\begin{quote}
athāvyaktavargādisamīkaranam/ tac ca \alert{madhyamāharanam} iti vyāvarnayanti ācāryāh/ yato 'tra vargarāśāv ekasya \alert{madhyamasyāharanam} iti/ atra sūtram vrttatrayam
\end{quote}
\begin{flushright}
--- (Hayashi 2010, p.47)
\end{flushright}
\end{column}
\begin{column}{.5\columnwidth}
\begin{quote}
Next equation involving the square or other [power] of the unknown is propounded.
[Its re-solution consists in] the elimination of the \alert{middle term}, as teachers of the sicence denominate it.
Here removal of one term, the middle one, in the square quantity, takes place: wherefore it is called elimination of the middle term.
\end{quote}
\begin{flushright}
--- (Colebrooke 1817, p. 207)
\end{flushright}
\end{column}
\end{columns}
\end{frame}
\begin{frame}[label={sec:org001d183}]{``A maxim of an original author''}
\begin{columns}
\begin{column}{.5\columnwidth}
\begin{quote}
atrādyaparibhāsā/
\end{quote}
\begin{verse}
'rāśiksepād vadhaksepo yadgunas tatpodottaram/\\
avyaktā rāśayah kalpyā vargitāh ksepavarjitāh/\\
\end{verse}
\begin{quote}
iyam kalpanā ganite 'tiparicitasya/

udāharanam
\end{quote}
\begin{verse}
ksetre tithinakhais tulye dohkotī tatra kā śrutih/\\
\alert{upapattiś ca rūdhasya ganitasyāsya kathyatām/}\\
\end{verse}
\begin{flushright}
--- (Hayashi 2010, p.55)
\end{flushright}
\end{column}

\begin{column}{.5\columnwidth}
\begin{quote}
So many times as the additive of the products contains the additive of the simple quantities, by the square-root of that [submultiple] as a common difference, the unknown quantities are to be put [in arithmetical progression] and to be squared, then diminished by substraction of the additive.\\

This supposition of apposite quantities required much dexterity in computations.\\

Example: Say what is the hypotenuse in a plane figure, in which the side and upright are equal to fifteen and twenty?
\alert{and show the demonstration of the received mode of computation.}
\end{quote}
\begin{flushright}
--- (Colebrooke 1817, pp.219-2020)
\end{flushright}
\end{column}
\end{columns}
\end{frame}
\section{Overview of the first procedure}
\label{sec:orgaf23c4c}

\begin{frame}[label={sec:orgfc55588}]{Let fall in the proposed triangle}
\begin{columns}
\begin{column}{.5\columnwidth}
\begin{quote}
atra karnah yā 1| etat-tryasram parivartya yāvattāvat-karno bhūh kalpitā| bhuja-kotī tu bhujau| tatra yo lambas tad-ubhayato ye tryasre tayor api bhuja-kotī pūrva-rūpe bhavatah| atas trairāśikam| yadi yāvattāvati karne 'yam 15 bhujas tadā bhuja-tulye karne ka iti labdho bhujah syāt
\end{quote}
\begin{flushright}
--- (Hayashi 2010, p.56)
\end{flushright}
\end{column}

\begin{column}{.5\columnwidth}
\begin{quote}
Here the hypotenuse is put /ya: 1.
Turning the triangle, the hypotenuse is made the base; \alert{and the side and upright in each of the triangles situated on either side of the perpendicular let fall in the proposed triangle, are analogous to the former.}
\end{quote}
\begin{flushright}
--- (Colebrooke 1817, pp.220-221)
\end{flushright}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[label={sec:orge1f0ed5}]{And so framing the equation}
\begin{columns}
\begin{column}{.5\columnwidth}
\begin{quote}
avādhā-yutir yāvattāvat-karna-samā kriyate| tāvad bhuja-koti-varga-yogasya padam karna-mānam upapadyate 25| anenotthāpite jāte āvādhe 9| 16| ato lambah 12| ksetra-darśanam|
\end{quote}
\begin{flushright}
--- (Hayashi 2010, p.56)
\end{flushright}
\end{column}

\begin{column}{.5\columnwidth}
\begin{quote}
The sum of the segments is equal to the hypotenuse: and so framing the equation, the value of the hypotenuse is deduced, the square-root of the sum of the squares of the side and upright, viz. 25.
Hence, subsituting the value, the segments are found 9 and 16; and thence the perpendicular 12. See
\end{quote}
\begin{flushright}
--- (Colebrooke 1817, p.221)
\end{flushright}
\end{column}
\end{columns}
\end{frame}
\section{Second proof}
\label{sec:orga507acf}
\begin{frame}[label={sec:org60ffd4f}]{Another proof}
\begin{columns}
\begin{column}{.5\columnwidth}
\begin{quote}
athānyathā kathyate
\end{quote}
\begin{flushright}
--- (Hayashi 2010, p.56)
\end{flushright}
\end{column}
\begin{column}{.5\columnwidth}
\begin{quote}
Or the solution is thus otherwise propounded.
\end{quote}
\begin{flushright}
--- (Colebrooke 1817, p.221)
\end{flushright}
\end{column}
\end{columns}
\end{frame}
\section{Questions}
\label{sec:org6d4f711}
\begin{frame}[label={sec:orge1357b4}]{Questions}
\begin{itemize}
\item Why go on to find the sides of the triangle?
\item How does this exemplify the rule?
\item Why two proofs?
\end{itemize}
\end{frame}
\end{document}