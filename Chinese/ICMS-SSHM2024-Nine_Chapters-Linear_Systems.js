const mainElement = document.getElementById("calculation");

const DIMENSIONS = [4, 3];

let nbStates = 0;

class Stage {
	constructor(name, children = []) {
		this.name = name;
		this.children = children;
		this.level = nbStates;
		nbStates += 1;
	}

	getTransitionFrom(state) {
		for(const transition of this.children) {
			if(transition.condition(state.value)) {
				console.log(transition.name);
				return transition;
			}
		}
		return null;
	}
}

class Transition {
	constructor(
		name,
		destination,
		condition = (value) => true,
		action = (value) => {}
	) {
		this.name = name;
		this.condition = condition;
		this.action = action;
		this.destination = destination;
	}

	transform(state) {
		const valueCopy = state.value;
		this.action(valueCopy);
		const level = this.destination.level > state.stage.level ?
			  state.level : state.level + 1;
		return new State(
			this.destination,
			valueCopy,
			level,
			state,
			this
		);
	}
}


class State {
	constructor(stage, value, level, previous = undefined, transition = undefined) {
		this.stage = stage;
		this._value = value;
		this.level = level;
		this.previous = previous;
		this.transition = transition;
		this.drawn = false;
		this.revealed = false;
	}

	createHTML() {
		if(!this.container) {
			this.container = document.createElement("div");
			const table = document.createElement("table");
			const body = document.createElement("tbody");
			const header = document.createElement("thead");
			const tr = document.createElement("tr");
			const th = document.createElement("th");
			const btn = document.createElement("button");
			th.colSpan = DIMENSIONS[1];
			btn.classList.add("card-btn");
			btn.onclick = this.reveal.bind(this);
			btn.innerHTML = this.stage.name;
			th.appendChild(btn);
			tr.appendChild(th);
			header.appendChild(tr);
			table.appendChild(header);
			table.appendChild(body);
			this.container.appendChild(table);
			this.cells = [...Array(DIMENSIONS[0]*DIMENSIONS[1])].map(
				x => document.createElement("td")
			);
			this.rows = [...Array(DIMENSIONS[0])].map(
				x => document.createElement("tr")
			);
			this.rows.forEach(
				(row, i) => [...Array(DIMENSIONS[1])].forEach(
					(col, j) => row.appendChild(this.cells[DIMENSIONS[1]*i+j]),
					this
				),
				this
			);
			this.rows.forEach(row => body.appendChild(row));
		}
		this.cells.forEach((cell, i) => {
			const value = this.value[Math.floor(i/DIMENSIONS[1])][i%DIMENSIONS[1]];
			cell.innerHTML = parseFloat(value.toFixed(2));
			cell.hidden = true;
		}, this);
		return this;
	}

	draw(parent) {
		this.createHTML();
		this.container.classList.add("procedure-step");
		this.container.style.position = "absolute";
		this.container.style.top = `${2 * DIMENSIONS[0] * this.stage.level}em`;
		this.container.style.left = `${2.5 * DIMENSIONS[1] * this.level}em`;
		if(!parent.contains(this.container)) {
			parent.appendChild(this.container);
		}
		this.parent = parent;
		this.updateParentHeight();
		this.drawn = true;
		return this;
	}

	updateParentHeight() {
		const height = this.container.offsetTop + this.container.offsetHeight;
		if (this.parent.offsetHeight < height) {
			this.parent.style.height = height + "px";
		}
	}

	reveal() {
		this.cells.forEach(cell => cell.hidden = false);
		this.container.scrollIntoView({behavior: "smooth"});
		if(this.next) {
			this.next.draw(this.parent);
		}
		this.container.scrollIntoView({behavior: "smooth"});
		this.updateParentHeight();
		this.revealed = true;
		return this;
	}

	get value() {
		return this._value.map(row => [...row]);
	}

	get next() {
		const transition = this.stage.getTransitionFrom(this);
		if (null === transition) {
			console.log("No transition found");
			return null;
		}
		return transition.transform(this);
	}
}

const CONDITIONS = {
	allNull: function(values) {
		for(const row of values) {
			for (const cell of row) {
				if (0 !== cell) {
					return false;
				}
			}
		}
		return true;
	},
	NOT_NULL: (row, col) => (values) => {
		return 0 != values[row][col];
	},
	NULL: (row, col) => (values) => 0 == values[row][col],
};

const ACTIONS = {
	MULTIPLY_COL: (col1, col2, row2) => (values) => {
		values.forEach(row => {
			row[col1] *= values[row2][col2];
		});
	},
	ELIMINATE: (col1, col2) => (values) => {
		values.forEach(row => row[col1] -= row[col2]);
	},
	DIVIDE: (row, col) => values => {
		const old = values.map(line => line.map(cell => cell));
		for(let i = 0; i < DIMENSIONS[0]; i++) {
			values[i][col] = values[i][col] / old[row][col];
		}
	},
};

function setUp() {
	nbStates = 0;
	const ROOT = new Stage("Enonciation");
	const HOMOGENEIZATION = [];
	const EQUALIZATION = [];

	for (let row = 0; row < DIMENSIONS[0] - 1; row++) {
		HOMOGENEIZATION.push([]);
		EQUALIZATION.push([]);
		const colRef = DIMENSIONS[1] - row - 1;
		for(let col = row + 1; col < DIMENSIONS[1]; col++) {
			const cell = [row, DIMENSIONS[1] - col - 1];
			HOMOGENEIZATION[row][col] = new Stage(
				`Homogeneization`
			);
			EQUALIZATION[row][col] = new Stage(
				`Equalization`
			);

			const equalize = ACTIONS.ELIMINATE(cell[1], colRef, cell[0]);

			HOMOGENEIZATION[row][col].children.push(new Transition(
				`Start equalizing ${row},${col}`,
				EQUALIZATION[row][col],
				CONDITIONS.NOT_NULL(cell[0], cell[1]),
				equalize
			));

			EQUALIZATION[row][col].children.push(new Transition(
				`Continue equalizing ${row},${col}`,
				EQUALIZATION[row][col],
				CONDITIONS.NOT_NULL(cell[0], cell[1]),
				equalize
			));
		}
	}

	const DIVISION = [];
	const FINAL_HOMOGENEIZATIONS = [];
	const FINAL_EQUALIZATIONS = [];

	for (let col = 0; col < DIMENSIONS[1]; col ++) {
		FINAL_HOMOGENEIZATIONS[col] = [];
		FINAL_EQUALIZATIONS[col] = [];
		for (let row = 0; row < col; row ++) {
			FINAL_HOMOGENEIZATIONS[col][row] = new Stage("Homogeneization");
			FINAL_EQUALIZATIONS[col][row] = new Stage("Equalization");
		}
		DIVISION.push(new Stage("Division"));
	}

	// Stage transitions

	ROOT.children.push(new Transition(
		"Initialization",
		HOMOGENEIZATION[0][1],
		(value) => true,
		ACTIONS.MULTIPLY_COL(1, 2, 0)
	));

	for (let row = 0; row < DIMENSIONS[0] - 1; row++) {
		for (let col = row + 1; col < DIMENSIONS[1]; col++) {
			const cell = [row, DIMENSIONS[1] - col - 1];
			const next = [
				col + 1 < DIMENSIONS[1] ? row : row + 1,
				col + 1 < DIMENSIONS[1] ? col + 1 : row + 2,
			];
			if (next[0] >= DIMENSIONS[0] - 2) {
				EQUALIZATION[row][col].children.push(new Transition(
					`Only the left low quality left`,
					DIVISION[0],
					CONDITIONS.NOT_NULL(DIMENSIONS[0]-2, 0),
					ACTIONS.DIVIDE(DIMENSIONS[0]-2, 0)
				));
				continue;
			}
			const colRef = DIMENSIONS[1] - next[0] - 1;
			const nextCell = [next[0], DIMENSIONS[1] - next[1] - 1];
			EQUALIZATION[row][col].children.push(new Transition(
				`${row},${col} equalized`,
				HOMOGENEIZATION[next[0]][next[1]],
				CONDITIONS.NOT_NULL(nextCell[0], nextCell[1]),
				ACTIONS.MULTIPLY_COL(nextCell[1], colRef, nextCell[0])
			));
		}
	}

	DIVISION[0].children.push(new Transition(
		`Last sequence of homogeneization`,
		FINAL_HOMOGENEIZATIONS[1][0],
		(value) => true,
		ACTIONS.MULTIPLY_COL(1, 0, DIMENSIONS[0]-2)
	));

	for (let col = 1; col < DIMENSIONS[1]; col ++) {
		for (let row = 0; row < col; row ++) {
			FINAL_HOMOGENEIZATIONS[col][row].children.push(new Transition(
				`Equalization for column ${col}`,
				FINAL_EQUALIZATIONS[col][row],
				(value) => true,
				ACTIONS.ELIMINATE(col, row)
			));

			FINAL_EQUALIZATIONS[col][row].children.push(new Transition(
				`Equalization for column ${col}`,
				FINAL_EQUALIZATIONS[col][row],
				CONDITIONS.NOT_NULL(DIMENSIONS[0]-col-1, col),
				ACTIONS.ELIMINATE(col, col-1)
			));

			if (row + 1 == col) {
				FINAL_EQUALIZATIONS[col][row].children.push(new Transition(
					`End of the equalization for ${col}`,
					DIVISION[col],
					(value) => true,
					ACTIONS.DIVIDE(DIMENSIONS[0]-col-2, col)
				));
			} else {
				FINAL_EQUALIZATIONS[col][row].children.push(new Transition(
					"Final division",
					DIVISION[col],
					CONDITIONS.NULL(row+1, col),
					ACTIONS.DIVIDE(DIMENSIONS[0]-col-2, col)
				));
				FINAL_EQUALIZATIONS[col][row].children.push(new Transition(
					"",
					FINAL_HOMOGENEIZATIONS[col][row+1],
					CONDITIONS.NOT_NULL(row+1, col),
					ACTIONS.MULTIPLY_COL(1,0, DIMENSIONS[0]-2)
				));
			}
		}

		if (col + 1 >= DIMENSIONS[1]) {
			continue;
		}

		DIVISION[col].children.push(new Transition(
			`Last sequence of homogeneization`,
			FINAL_HOMOGENEIZATIONS[col + 1][0],
			(value) => true,
			ACTIONS.MULTIPLY_COL(1, 0, DIMENSIONS[0]-2)
		));
	}
	return ROOT;
}


function printStageChilds(stage) {
	if (stage.children.length < 1) {
		return;
	}
	for(const transition of stage.children) {
		if(transition.destination.level > stage.level) {
			console.group(stage.name, "->", transition.name, "->", transition.destination.name);
			printStageChilds(transition.destination);
			console.groupEnd();
		} else {
			console.log(stage.name, "->", transition.name, "->", transition.destination.name);
		}
	}
}

let values = [
	[1, 2, 3],
	[2, 3, 2],
	[3, 1, 1],
	[26, 34, 39],
];

function updateTable() {
	const ROOT = setUp();
	mainElement.innerHTML = "";
	const prev = values.map(row => row.map(cell => cell));
	values = [];
	const number = document.getElementById("typeNumberSelector");
	const table = document.getElementById("setSystem");
	const body = table.querySelector("tbody");
	body.innerHTML = "";
	for (let row = 0; row < parseInt(number.value) + 1; row ++) {
		const tr = document.createElement("tr");
		values.push([]);
		const th = document.createElement("th");
		if (row < number.value) {
			th.innerHTML = `Type ${row+1}`;
		} else {
			th.innerHTML = "Products";
		}
		tr.appendChild(th);
		for (let col = 0; col < number.value; col ++) {
			const td = document.createElement("td");
			const input = document.createElement("input");
			input.type = "number";
			input.name = `system[${row}][${col}]`;
			if (row < prev.length && col < prev[row].length) {
				input.value = prev[row][col];
				values[row].push(prev[row][col]);
			} else {
				input.value = 0;
				values[row].push(0);
			}
			input.addEventListener(
				"change",
				event => {
					mainElement.innerHTML = "";
					values[row][col] = parseInt(event.target.value);
					let state = new State(ROOT, values, 0);
					state.draw(mainElement);
				}
			);
			td.appendChild(input);
			tr.appendChild(td);
		}
		body.appendChild(tr);
	}
	const state = new State(ROOT, values, 0);
	state.draw(mainElement);
	console.log(mainElement);
}

const dimensionSelector = document.getElementById("typeNumberSelector");
dimensionSelector.addEventListener(
	"change",
	event => {
		DIMENSIONS[0] = parseInt(event.target.value) + 1;
		DIMENSIONS[1] = parseInt(event.target.value);
		console.log(DIMENSIONS);
		updateTable();
	}
);
DIMENSIONS[0] = parseInt(dimensionSelector.value) + 1;
DIMENSIONS[1] = parseInt(dimensionSelector.value);
updateTable();


/*
let iteration = 0;
do {
	state.draw(mainElement);
	state = state.next;
	iteration++;
} while(null !== state && 100 > iteration)
*/
